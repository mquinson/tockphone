# TockPhone

L'objectif est de faire un téléphone capable de rendre certains services d'un smartphone, mais avec une semaine d'autonomie par charge et une durée de vie d'une
dizaine d'années.

**À propos du document.** Ce document contient mes réflexions à l'instant T. Le contenu peut changer d'une fois sur l'autre, il y a forcément des choses fausses :
tout est nouveau pour moi ici, et le document reflète aussi l'état de mes erreurs actuelles :)

**État du projet.** Pour l'instant je lis beaucoup pour tenter de comprendre, car je n'y connais rien. J'en parle à qui veut bien m'écouter: si vous avez un avis ou
une suggestion, n'hésitez pas à [me contacter](http://people.irisa.fr/Martin.Quinson/Contact). Mais je n'ai rien construit, pas écrit de code. Je n'ai même pas encore
acheté de matos. Ça ne saurait tarder.

[TOC]


## Services visés

On parle d'un téléphone offrant les services suivants :
 - téléphone, SMS (+vocaux?)
 - messagerie rapide avec matrix, mastodon
 - lire des epub, navigation gemini / web sans javascript
 - Doc offline aux formats [kiwix](https://www.kiwix.org) et [dash](https://kapeli.com/dash)
 - jouer des fichiers audio mp3/ogg/opus
 - prise de notes légères (TODO et liste de courses)
 - Afficher des cartes GPS (si le le modem GSM fait effectivement ça)
 - Authentification 2 facteurs type FreeOTP (pas suffisant pour les banques. https://mastodon.social/@mmu_man@m.g3l.org/105701988807156810)

Services exclus à priori:
- vidéo et visioconf, applis web évoluées (trop gourmand en calcul)
- dessin vectoriel, tableur, compiler du markdown en livre (mauvaise taille d'écran)
- (retro)gamming (écran trop lent)
- Applis tierces (RATP, ma banque, musée): pas de compatibilité possible

## Motivation

La première motivation est tout d'abord de voir si c'est possible, de résoudre un puzzle rigolo. Ensuite, on cherche à imaginer une informatique minimaliste et
conviviale (à la fois dans le sens "utilisable" et dans le sens d'Illich de "maîtrisable par tous"). Il n'y a pas de bizplan ni de mesure d'impact, mais on cherche
une alternative aux bloatwares de l'informatique moderne pour inventer un futur enviable, une utopie à viser.

Le projet peut sembler fou à priori, mais les premiers téléphones DIY à base de raspy ont une dizaine d'années:
[zerophone](https://www.crowdsupply.com/arsenijs/zerophone) [noname1](http://www.davidhunt.ie/piphone-a-raspberry-pi-based-smartphone)
[noname2](https://www.maxigadget.com/2015/04/tuto-fabrication-smartphone-raspberry-pi.html). Il s'agit juste de faire un pas de plus, si possible. Par ailleurs, le
[Light Phone](https://www.thelightphone.com) tient un jour ou deux d'usage en charge avec une batterie quatre fois plus petite qu'un FairPhone4 (950mAh contre
4000mAh).

# Principe et vision générale

## Un microcontroleur en guise de CPU

Pour tenir une semaine de charge, il faut un CPU minimaliste. Il est impossible d'embarquer un linux sur un raspy, ou alors il faut tricher en mettant 4kg de
batteries. Il faut donc viser un microcontrôleur (MCU). L'espoir est que les MCU récents soient assez rapides pour tourner les services visés, mais c'est à confirmer
en essayant.

### Pb #1: pas de séparation entre programmes
Pas de [MMU](https://en.wikipedia.org/wiki/Memory_management_unit) pour compartimenter les espaces d'adressage entre les programmes, et pas de contextes d'exécution
séparés. Sur les petits MCU, il n'y a donc pas de différence entre programme et OS, et une erreur dans un composant peut tout faire planter.

Les MCU un peu plus gros ont une [MPU](https://en.wikipedia.org/wiki/Memory_protection_unit) qui consiste à spécifier des plages d'adresses mémoires interdites à
chaque application. Les accès intempestifs lèvent une interruption équivalente à la faute de page, qui rend le contrôle à l'OS embarqué. On contrôle ainsi finement
les autorisations d'accès aux quelques kilos de mémoire disponibles. Les MCU sans MPU sont trop petits pour tourner les services qu'on vise, de toute façon.

### Pb #2: vitesse de calcul
C'est plusieurs ordres de grandeur inférieur à un petit CPU comme raspberry, car le processeur est très très simple (pour être bon marché, ce qui est la motivation
habituelle des acheteurs de MCU) et cadencé à fréquence assez faible en comparaison.

### Pb #3: très peu de mémoire vive
Les plus gros MCU semblent avoir environ 256kb ou 512kb de RAM et 1 ou 2Mb de flash, et l'absence de MMU empêche de tricher avec un swap sur disque. Pour comparaison,
helloword.c compilé par gcc sans option linux, c'est 16 ko. Linux est [devenu fou](http://timelessname.com/elfbin/), avec [le
temps](https://nathanotterness.com/2021/10/tiny_elf_modernized.html).

### Matériel choisi
À priori, on partirait sur un [STM f446re](https://www.st.com/en/evaluation-tools/nucleo-f446re.html#st_all-features_sec-nav-tab), un gros MCU plus ou moins
[supportés par TockOS](https://github.com/tock/tock/blob/master/boards/README.md). On trouve toute la doc et les cartes de dev sans problème. Le MCU de la famille
Raspberry, le [pico](https://core-electronics.com.au/guides/raspberry-pi-pico/raspberry-pi-pico-w-overview-features-specs/**, est quand même un peu petit.

**TODO: paragraphe en travaux*** Je pense que ça peut faire le taff, car ce MCU [fait 225 DMIPS et 608
CoreMark](https://www.st.com/en/microcontrollers-microprocessors/stm32f446.html). D'après [ces listes](https://www.eembc.org/coremark/scores.php), un Pentium 6 sorti
en 2009 faisait 889 CoreMark Pour info, un SnapDragon 865 qu'on trouve dans certains smartphones a environ 10 milliards de transistors (84mm² à 7nm) sans compter la
mémoire vive. Un Cortex-M4 comme dans le f446 semble avoir ~180k portes, ie 700,000 transistors, et pas forcément de RAM à coté. Le Cortex-M0 du pico a plutôt 25k
portes, ie 100,000 transistors, sans RAM. Ces chffres sont à prendre avec des pincettes, c'est une info difficile à trouver de façon sure, je suis preneur d'avis
sourcés. Le bon vieux processeur 68k avait 68k transistors.

Niveau benchmarks, le STM32F4 plafonne à 225 DMIPS/608 CoreMark à 180 MHz. Sa conso varie entre 89 µA/MHz pour le STM32F410 à 260 µA/MHz pour le STM32F439.

## Fondation logicielle en Rust

Le langage et l'outillage sont utilisés par de nombreux projets visant l'embarqué, donc on est à priori sur les épaules du bon géant.

### Sureté à la compilation

Le projet [TockOS](https://www.tockos.org) vise à faire un OS robuste pour MCU. Le [design](https://www.tockos.org/documentation/design) fait la différence entre les
modules noyau (nommé capsules) qui doivent être écrits en Rust pour interdire les accès mémoire invalide, n'ont pas le droit d'allouer dynamiquement de la mémoire, et
sont schedulés de manière collaborative. Les vérifications à *compile time* de Rust appliquées à tout le kernel (drivers compris) devrait protéger contre les blue
screen of death. Les programmes Rust paniquent, mais ils n'écrivent pas n'importe où en mémoire physique, déjà.

Les applications quant à elles peuvent être écrites dans n'importe quel langage, et TockOS utilise automatiquement le MPU du microcontroler pour s'assurer qu'elles
n'accèdent pas n'importe où en mémoire. Elles ont une pile et un tas alloués dynamiquement, et de classiques interruptions matérielles redonnent régulièrement le
contrôle au noyau pour éviter qu'une appli en boucle infinie ne gèle le système. Si une capsule a besoin de mémoire pour rendre un service à une appli appelante, la
capsule prend cette mémoire dans une zone réservée à l'appli. Cela assure qu'une éventuelle fuite mémoire du noyau se résoudra en relançant l'appli concernée.

Ce [design](https://www.tockos.org/documentation/design) est élégant, bien adapté aux spécificités des MCU et tire parti de Rust. Rien n'empêche les capsules d'un bug
de liveness (deadlock ou boucle infinie), mais on peut imaginer que les async/await de Rust peuvent aider si elles sont utilisées.

Une limitation potentielle, c'est que même les drivers matériels doivent être en Rust, mais les drivers de composants simples (comme un petit écran pour raspberry)
doivent avoir un comportement mémoire assez simple. La conversion C->Rust de ces pilotes ne devrait pas poser de problème. De toute façon, les composants plus
complexes comme la pile réseau sont déjà supposés être implémentés comme des applications séparées du noyau TockOS.

### Framework frugal

Les capacités matérielles visées interdisent strictement d'utiliser les environnements graphiques habituels comme Gnome/Qt, mais on ne peut pas imaginer un téléphone
en ligne de commande.

Le projet [Slint](https://slint-ui.com) vise à simplifier l'écriture d'applications graphiques peu gourmandes. On écrit l'interface dans un [fichier
texte](https://github.com/slint-ui/slint/blob/master/examples/memory/memory.slint) qui liste les widgets et leur position, et qui connecte les signaux aux callbacks
du programme. Ensuite on programme les callbacks en Rust ou C++, puis on compile tout ça. Slint génère un binaire compilé d'après la description textuelle. Cf [le
tuto](https://slint-ui.com/releases/0.3.1/docs/tutorial/rust/) adéquat.

C'est donc un peu comme Glade du projet Gnome (voire comme HTML+javascript) où l'interface graphique est construite d'après une description textuelle. La grosse
différence est que c'est intégralement compilé, il n'y a pas d'interpréteur d'interface au runtime. D'une certaine façon, Glade est comme WordPress à retraduire tout
le markdown à chaque usage, tandis Slint est comme Jekyll qui compile le markdown à l'avance pour faire un site HTML statique.

En plus, la dernière version de Slint vise les MCU ([demo](https://slint-ui.com/blog/slint-0.3-released-with-platform-api-for-mcu.html),
[doc](https://slint-ui.com/releases/0.3.0/docs/rust/slint/docs/mcu/index.html)) de la taille de ceux qu'on loger dans l'enveloppe énergétique choisie.

### Thin client

Si Slint est encore trop gourmand pour loger tous les services sur un MCU (ou juste pour pousser l'idée plus loin), on pourrait séparer d'une part le serveur qui rend
un service (jouer la musique) et d'autre part l'interface qui contrôle le service, afin de ne charger l'interface en mémoire qu'au besoin.

C'est un peu comme un thin client des systèmes distribués du siècle dernier, où l'UI tourne sur un ordi bon marché tandis que l'appli tourne sur un système distant.
Mais ici, les deux parties sont sur la même machine. Ca doit être implémentable en ajoutant des RPC dans Slint, dans l'esprit de [ce
papier](https://www.researchgate.net/profile/Qi-Wang-295/publication/236875482_RemoteUI_A_High-Performance_Remote_User_Interface_System_for_Mobile_Consumer_Electronic_Devices/links/02e7e51a8bc80b4093000000/RemoteUI-A-High-Performance-Remote-User-Interface-System-for-Mobile-Consumer-Electronic-Devices.pdf).

Ce modèle est utilisé dans [Music on Console](http://moc.daper.net), un lecteur audio dans le terminal. On tape 'mocp' dans le terminal pour ouvrir l'interface, on
choisi une autre chanson puis on quitte l'interface et retourne au terminal pendant que le serveur joue la chanson.

Séparer la logique applicative de l'interface d'affichage aurait de nombreux avantages, même sous linux. On réduirait la duplication des efforts entre les programmes
Gnome et KDE, si le coeur était partagé entre les différentes interfaces. L'éditeur de texte [Xi](https://xi-editor.io) sépare ainsi front-end et back-end (dommage
que son developpement soit arrêté). L'idée ici est d'utiliser Slint pour faire la même chose de façon générique pour toutes les applications [qui utilisent Slint].
Faire un protocole spécifique à chaque application a sans doute des avantages (les gens de Xi militent aussi pour [protocols, not
plugins](https://xi-editor.io/docs/plugin.html)), mais l'abstraction offerte par Slint semble bien adaptée.

Comme avec les clients légers classiques, on pourrait utiliser cette idée pour déporter des applications hors du TockPhone pour leur donner de la puissance de calcul,
ou au contraire contrôler les applications du téléphone depuis un vrai clavier. La latence réseau complique habituellement l'utilisation d'applications à distance,
mais ça peut être utilisable si les widgets sont relativement indépendants. Il s'agirait d'étendre ce que [mosh](https://mosh.org/) fait pour le terminal à tous les
widgets de Slint. Disons que ce n'est pas pour la v1 du TockPhone.

La dernière idée folle offerte par la découpe de l'application et de sa logique sur la ligne de Slint, ce serait de pouvoir faire plusieurs interfaces pour le même
coeur applicatif. Changer toutes les applications en des bibliothèques rendant des services utilisés par les interfaces pour faire le travail. C'était le principe
d'[AppleScript](https://en.wikipedia.org/wiki/AppleScript), où toutes les applications du système étaient scriptables pour être facilement assemblable, remixable,
etc.

### Textual UI ?

Faire des interfaces textuelles est une approche classique pour lutter contre le bloatware. L'application présente des boites, des menus et des fenêtres, mais tout
dessiné avec des lettres pour faire léger (à l'oeil et pour l'ordi) au lieu de faire zoli.

On pourrait ajouter un backend Slint ciblant les interfaces textuelles avec [cursive](https://github.com/gyscos/cursive) (un crate Rust proposant des widgets prêts à
l'emploi pour les TUI). La bonne nouvelle étant qu'il y a beaucoup d'applications TUI en Rust. [termusic](https://github.com/tramhao/termusic) pour la musique (avec
support du téléchargement automatique), [Ox](https://github.com/curlpipe/ox) et [Xi](https://xi-editor.io/) pour les éditeurs de texte,
[rexcel](https://github.com/saulane/rexcel) pour lire des fichiers excel en TUI, etc.

Ce type d'interface n'est pas forcément adapté à un tout petit écran comme sur téléphone, mais l'idée reste intéressante si on faisait une sorte de TockOrdi. Par
exemple en utilisant une [uConsole](https://www.clockworkpi.com/uconsole) ou au moins ce genre de boitier avec un MCU dedans :)

## Écran très basse consommation

### Besoin
L'enveloppe énergétique demande d'aller à contre sens de la tendance qui dote le [Fairphone 4](https://shop.fairphone.com/en) d'une dalle 6.3 pouces ultra lumineuse
(et de la 5G avec deux caméras de 48MP). À la place, réduire la taille de l'écran pour remettre un clavier mécanique comme sur les téléphones d'il y a 10 ou 15 ans.
Un tel clavier prend un peu de place, mais il ne consomme quasi rien. L'écran est plus petit et consomme moins. En plus, il n'a pas besoin d'être touchscreen (je ne
sais pas si un touchscreen consomme plus mais en tout cas, ça joue sur le prix).

Pour bien faire, il faut prendre un écran à encre numérique (e-Ink), comme une liseuse. Physiquement, ce sont des petites billes qui changent d'orientation à la
demande pour montrer différentes couleurs. Cela ne consomme que lorsqu'on tourne les billes pour changer ce qui est affiché, mais ensuite cela reste affiché
indéfiniment sans aucune consommation d'énergie. [Cette vidéo](https://www.youtube.com/watch?v=MsbiO8EAsGw) explique le principe en détail. Certains composants
envisagés pour l'écran du TockPhone sont habituellement vendus pour faire des étiquettes de prix 6x8cm dans les rayons des super marchés (ce monde est fou).

### Existant
Le gros problème de ces écrans, c'est que le taux de rafraichissement peut monter à 30 secondes sur [certains
composants](https://www.waveshare.com/4.01inch-e-Paper-HAT-F.htm)... Ne pas pouvoir jouer une vidéo en 120 FPS est une chose, mais c'est quand même bien d'afficher le
numéro appelant avant que l'autre ne raccroche. À priori, ce n'est pas une limitation intrinsèque à l'encre numérique, puisque ce [téléphone
chinois](https://www.youtube.com/watch?v=XgC0SB4EFw0) tourne à 30fps (au passage, ce téléphone est une véritable [débauche
technologique](https://www.gsmchoice.com/en/catalogue/hisense/a9/) octo-coeurs et GPU. Aussi énergivore que les autres smartphones malgré son écran eInk).

Les écrans disponibles chez Adafruit ou [WaveShare](https://www.waveshare.com/product/raspberry-pi/displays/e-paper.htm) ont des taux de rafraichissement très
différents. Je ne suis pas sûr de tout comprendre, mais voici quelques tentatives d'explications. La première chose à comprendre, c'est que ces écrans viennent avec
leur propre mémoire. C'est bon pour les MCU qui sont limités de ce coté, et la physique de l'eInk semble compliquée. Donner de la RAM à l'écran évite de tout devoir
comprendre.

Les plus grands écrans eInk d'Adafruit font 2.5 pouces. Par exemple [celui-ci](https://www.adafruit.com/product/4195) (2.13 pouces: 6x4cm -- 250x122 pixels
monochrome, pour 23$) semble pas mal. Il a une mémoire embarquée pour sauver les 8ko correspondants. C'est trop petit.

Le choix est bien plus large (et mieux documenté) chez WaveShare. Par exemple
[celui-ci](https://www.waveshare.com/product/displays/e-paper/epaper-2/3.7inch-e-paper-hat.htm) fait 3.7 pouces (4.7x8cm -- 480×280 pixels à 4 niveaux de gris, 28$)
[vient avec sa RAM embarquée](https://www.waveshare.com/w/upload/2/2a/SSD1677_1.0.pdf). Un rafraichissement complet de l'écran prend 4 secondes, mais il sait faire
des rafraichissements partiels en un tiers de seconde. C'est lent, probablement parce qu'on lui parle en
[SPI](https://learn.sparkfun.com/tutorials/serial-peripheral-interface-spi/all) comme à la plupart des écrans waveshare de moins de 5 pouces, mais je ne suis pas sûr
de tout comprendre.

[Celui-là](https://www.waveshare.com/4.3inch-e-Paper.htm) fait 4.3 pouces (88x66mm -- 800x600 pixels 4 niveaux de gris pour 54$) redessine tout en 1.5 secondes
seulement (sans MAJ partielle possible). On lui parle avec la technologie [UART](https://learn.sparkfun.com/tutorials/serial-communication/uarts), ce qui doit
expliquer des choses. Mais ce qui m'étonne, c'est qu'on ne semble pas pouvoir accéder directement à la mémoire dessinée. Il faut passer par un [protocole de
communication](https://www.waveshare.com/wiki/4.3inch_e-Paper_UART_Module#Serial_communication_protocol) pour lui donner les consignes de quoi dessiner où. Porter
Slint (ou autre) à ce protocole semble relativement complexe, non?

Et certains écrans plus grands se redessinent en moins d'une seconde, comme [celui-ci](https://www.waveshare.com/6inch-HD-e-Paper-HAT.htm) (6 pouces: 12x9cm,
1448x1072 pixels en 16 niveaux de gris pour 118$) ou [celui-là](https://www.waveshare.com/13.3inch-e-Paper-HAT.htm) (carrément 13 pouces: 27x20cm, 1600×1200 pixels à
16 niveaux de gris). Comment est-ce possible ? Et bien ces écrans viennent avec ... une sorte de gpu. Waveshare vend d'ailleurs séparément ces [composants
dédiés](https://www.waveshare.com/product/displays/e-paper/driver-boards.htm) au pilotage d'un écran eInk. Le [IT8951](https://www.ite.com.tw/en/product/view?mid=95)
vient par exemple avec 64Mo de SDRAM, une connectique rapide et sait appliquer quelques effets à l'image (pour comparaison, le F446 -- le MCU choisi pour le coeur du
TockPhone -- a 128ko de RAM). C'est beau dites vous? Oui, mais ça consomme. Le 6 pouce ci-dessus consomme (écran + contrôleur) 0.3W au repos et 0.9W quand on dessine.
L'écran en SPI ci-dessus consomme 50mW quand on dessine et rien de mesurable au repos. Celui en UART consomme moins de 5mA au repos et sa conso active ne semble pas
documentée.

Tout ceci est un peu agaçant, car on sent bien qu'avec une intégration un peu supérieure entre les composants, on aurait un écran eInk avec un taux de
rafraichissement très satisfaisant. Mais on ne va pas aller bidouiller les contrôleurs de ces écrans. En tout cas, certainement pas pour l'instant.

### Choix

Difficile de dire si l'usage est plus agréable avec un full update en 3 secondes + partial update en 0.3sec, ou bien un full update en 1.5 secondes sans partial
update. Je pense commencer avec le 3.7 pouces pour son prix, son encombrement et sa conso.

[L'écran 3.7 pouces](https://www.waveshare.com/product/displays/e-paper/epaper-2/3.7inch-e-paper-hat.htm) (Outline: 58x96mm Affichage: 4.7x8cm -- 480×280 pixels à 4
niveaux de gris, 28$). Il est un poil petit, mais moins cher et consomme vraiment rien (50mW quand on dessine).

[L'écran 4.3 pouces](https://www.waveshare.com/4.3inch-e-Paper.htm) (Outline: 118×75mm; Affichage: 88x66mm -- 800x600 pixels monochromes pour 54$, refresh 1.5 sec).
Il est peut-être un poil gros, une capacité d'update partielle aurait été pratique.

Dans les deux cas, ces écrans ne simplifient pas le dev, mais on a rien sans rien de nos jours.

### TODO

- Regarder de plus près les écrans [GooDisplay](https://www.good-display.com/product/437.html) (3.7 pouces, MAJ rapide en noir et blanc pour
  [16$](https://buy-lcd.com/products/371-inch-eaper-display-black-and-white-e-ink-screen-module-gdew0371w7). Le pb est que la lifetime annoncée est "1000000 times or
  5years" (page 32), donc ça tient pas 10 ans. Si le waveshare est pareil, on a un pb.

- D'après [ce tuto du constructeur](https://www.silabs.com/documents/public/white-papers/implementing-low-power-graphical-displays.pdf), le MCU EFM32 semble savoir
  piloter les EPD (electronic paper displays). C'est de la pub, mais à fouiller.


## Clavier séparé

### Besoin
Le clavier n'est pas si simple qu'il n'y parait. Ce n'est pas urgent, car la V0 peut très bien se tester sur board de dev avec un clavier USB 104 touches mécaniques.

### Existant
Les plans de fabrication d'un autre téléphone OpenHardware (le [Precursor](https://www.crowdsupply.com/sutajio-kosagi/precursor), orienté sécurité) sont [dispos en
ligne](https://github.com/betrusted-io/betrusted-hardware/tree/main/kbd-v3). Malheureusement, c'est [affreusement compliqué à
fabriquer](https://github.com/betrusted-io/betrusted-hardware/discussions/6). Cette voie semble bouchée.

Sinon, le [clavier Ginni](https://www.gboards.ca/product/ginni) est amusant aussi : il n'y a que 10 touches, mais avec le [layout ASTENIOP](https://asetniop.com/) on
peut taper toutes les lettres, la ponctuation et meme des mots prêts à l'emploi [en pressant plusieurs touches à la fois](https://asetniop.com/layouts). Rigolo (avec
ça, t'as pas besoin de protéger ta machine par un mot de passe), mais c'est juste un autre projet. Hors sujet.

Plus sérieusement, il semblerait que beaucoup de [projets](https://github.com/Dakkaron/Fairberry/)
[DIY](https://www.tindie.com/products/arturo182/bb-q20-keyboard-with-trackpad-usbi2cpmod/) utilisent les claviers blackberry, qui sont encore en vente sur AliExpress.
2.5$ pour le [Q10 à 35 touches](https://www.makerfabs.com/keypad-for-blackberry-q10.html) (67x32mm) ou bien 7$ pour le [Q20 à 40
touches](https://fr.aliexpress.com/item/1005004675849801.html). Faire une [interface
I2C](https://www.hackster.io/news/convert-a-blackberry-qwerty-keyboard-to-an-i2c-device-8c059b59ab70) semble facile (autre
[projet](https://www.tindie.com/products/arturo182/bb-q10-keyboard-pmod/)). On trouve aussi des [claviers 12
touches](https://fr.aliexpress.com/item/1005004693984518.html) de type 3310 pour 2.5$.

Pour s'amuser, on a acheté [un kit oshw de clavier Q20](https://lectronz.com/products/bb-q20-keyboard-with-trackpad-usb-i2c-pmod) qui devrait faciliter la mise en oeuvre à court terme (interface série toute belle et facile).

## Modem voix+4G
### Besoin
Un téléphone a bien sûr besoin du réseau GSM pour passer et recevoir des appels. Quand on vise à rester utilisable 10 ans, il faut viser la 4G car les opérateurs
semblent dire que ce protocole là est parti pour rester (au contraire de la 5G qui pourrait être remplacée par la 6G). Les antennes 2G (celles des vieux téléphones
raspy) et 3G (en vente chez Adafruit) sont déjà en voie d'extinction.

On veut donc un modem qui fasse GSM (pour passer la voix de partout, et les SMS) + LTE (aka 4G) basse conso. On voudrait pouvoir éteindre le LTE ou au moins le mettre
en veille profonde très souvent, mais on voudrait que le GSM reste tout le temps allumé. Savoir utiliser les autres générations de réseau (5G) n'est pas important.

**Le modem est probablement le point matériel le plus difficile du TockPhone, puisqu'il n'existe pas vraiment de board adaptée dans le commerce**.

### Existant
On pourrait prendre un modem classique, comme le [EC21-G](https://www.quectel.com/wp-content/uploads/2021/03/Quectel_EG21-G_LTE_Standard_Specification_V1.3.pdf) de
Quectel. Ce bijou fait tout, et le reste. Encodage de la voix (avec une demi-douzaine de codecs) avec suppression de bruits parasites, GSM/EDGE/GPRS/3G/4G tous
continents, GPS compatibles tous fournisseurs, connectique de fou... pour la modique somme de 80€. Je ne vois pas les infos de conso électrique, mais c'est
probablement trop gros pour ce qu'on cherche. Pourtant, ça semble être le plus petit de sa catégorie chez ce vendeur.

À l'autre extrémité, les composants très basse conso visent l'IoT, et ils ont donc plutôt des variantes de LTE bas débit comme LTE-M. Pas grave: on a pas besoin de
150Mb/s quand on a 0.3 FPS sur l'écran. Le fait qu'il n'écoute pas le réseau en continu mais passe en veille le plus souvent est plutôt une fonctionnalité à mes yeux
(tant que le GSM reste connecté). Mais c'est là que le bat blesse : je ne trouve pas de modem LTE-M qui fasse aussi GSM pour la voix. Il y a bien le standard VoLTE
qui permet de passer de la voix sur IP sur le réseau 4G, mais je voudrais une large couverture pour les appels voix. Par exemple dans cette catégorie, le
[nRF9160](https://www.nordicsemi.com/Products/nRF9160) de NordicSemi est amusant: c'est [SIP](https://fr.wikipedia.org/wiki/System_in_package) contenant un MCU assez
comparable au F446 que l'on vise et un petit modem LTE. Intéressant, mais dommage que le modem embarqué ne fasse pas GSM.

Le [Lara série L6](https://www.u-blox.com/en/product/lara-l6-series) de chez U-blox sait faire la voix, et ils se vantent d'avoir la plus petite puce du marché
sachant faire ça. Mais je ne trouve pas d'info de conso électrique alors que le datasheet dit que la puce n'implémente pas encore très bien le mode économie
d'énergie. Surtout, la puce n'est pas en vente au public. Il faut les contacter pour négocier.

Une piste attractive serait de voir si on arrive à faire de la voix avec un modem ultra classique comme un [Quectel
EC200T](https://www.mc-technologies.net/bilder-downloads/datenkommunikation/PDF-Dateien/Quectel_EC200T_Series_LTE_Standard_Specification_V1.1.pdf) (environ 45€ pour
la version EU qui marche en europe). Le datasheet officiel de cette puce dit qu'elle ne fait que LTE data sans voix (et GPS), mais le [manuel
détaillé](https://forums.quectel.com/uploads/short-url/dV5cK9eteeQmwyGPgfWB351oZde.pdf) du protocole série laisse entendre que le modem implémente des instructions
pour la voix (chapitre 7: Call Related Commands). Soit ces fonctionnalités sont embarquées sur toutes les puces EC200T et on va pouvoir les utiliser qu'elles soient
documentées ou non, soit c'est en option et j'ai pas compris ce qui se passe.

Vu que cette puce est produite en très grand volume et que ce constructeur a l'habitude d'assurer la compatiblité ascendante de ses puces (pour que les nouvelles
remplacent les anciennes dans les design existants) ce serait vraiment bien d'arriver à faire ça :crossed_fingers:

Update: avec le bon firmware, le module en question a réussi à passer/recevoir des appels voix et envoyer/recevoir des SMS depuis la carte d'évaluation. Good enough!

## Wifi (à faire)
Une connexion wifi serait aussi bienvenue pour un smartphone, ainsi que la possibilité de servir de point d'accès 4G->wifi pour un appareil à coté, mais pas tout de
suite: faire marcher un modem 4G semble plus urgent sur un téléphone.

Pour l'instant, j'ai juste fait un peu de biblio et je suis tombé sur [cette étude](https://blog.voneicken.com/projects/low-power-wifi-intro), qui me semble bien
faite. Ce que je trouve dingue dans ce quartier, c'est que (1) toutes les puces wifi proposées ont en fait à peu près la même puissance de calcul que le 446 qui nous
sert de CPU (2) un tel modem wifi coûte environ 3€, en gros le tiers du prix du 446.

On pourrait remplacer le 446 par un ESP32 avec une zone wifi embarquée, mais ce chip n'aura probablement pas la puissance de calcul nécessaire pour faire à la fois
l'interface et les communications wifi. Il faut donc partir sur un design à deux puces. Pour les faire communiquer en ultra low power, [SPI semble le plus
adapté](https://ntnuopen.ntnu.no/ntnu-xmlui/bitstream/handle/11250/2456608/16865_FULLTEXT.pdf) à défaut de Mbus. Cette étude me sera utile quand je voudrais
comprendre comment SPI fonctionne en multi-master.

Une fois qu'on aura 2 MCUs dans la boite, l'idée de faire du thin client prendra probablement une autre saveur, mais il faut faire attention à ne pas réveiller tous
les composants du boitier chaque fois qu'on a un événement à traiter.

## Biblio à fouiller
### Autres projets d'inspiration
- [UltimateTyper](https://alternativebit.fr/posts/ultimate-writer/) une pièce détachée de kindle comme écran EPD, un clavier et un Raspy dans une boite en bois. Joli, qques infos sur les EPD.
- [Prose](https://medium.com/this-should-exist/prose-a-distraction-free-e-ink-laptop-for-thinkers-writers-4182a62d63b2) un rêve de la machine idéale pour écrire sans être distrait. Liste d'appli intéressantes. Ca reste un peu éthéré, tout comme la [machine pour 50 ans de ploum](https://ploum.net/the-computer-built-to-last-50-years/). [FreeWrite](https://getfreewrite.com/) est en vente, pour la modique somme de 500 balles.
- [The 100 years computer](https://thedorkweb.substack.com/p/the-100-year-computer) Après une vision poétique un peu longue, ce projet propose une implémentation de CP/M (un OS des années 80) sur STM32 car on trouve ces puces partout. [La suite](https://thedorkweb.substack.com/p/more-on-heirloom-computing) contient une sorte de FAQ de toutes les remarques reçues. Ce projet est amusant car c'est qqun du matériel qui fait des expériences parfois malheureuses en logiciel (comme envisager BASIC comme base). Nos erreurs sont complémentaires. Perso je veux utiliser la pointe de la technologie logicielle (Rust) pour faire un OS simple et novateur sur ces puces qu'on trouve partout. Il a l'avantage d'un écosystème de logiciels existants. J'espère avoir l'avantage de profiter de trois décénies de recherches potentiellement utiles, pour qqch plus simple à maintenir.
- Possibilité de recharge par manivelle ou panneaux solaires: https://mastodon.social/@bohwaz@mamot.fr/109387665166783286  https://mastodon.social/@flomaraninchi@pouet.chapril.org/109387687203507459
- Pourquoi on change de téléphone au fait? https://mastodon.social/@MartinShadok@aleph.land/109388017228969090
### Outils potentiellement intéressants
- [MCUfont](https://github.com/mcufont/mcufont), un outil de compression/décompression de police de caractères adapté aux contraintes mémoires des MCU.
- [uGFX](https://ugfx.io) une bibliothèque graphique adaptée aux MCU. Double licence libre + payante pour usage commercial.
- [GxEPD2](https://github.com/ZinggJM/GxEPD2) une bibliothèque de rendu spécifique aux EPD vendus par WaveShare et GooDisplay.
- [Discussion](http://essentialscrap.com/eink/software.html) sur la bonne façon de faire du rendering sur EPD (list display) par opposition au frame buffering.
# La suite
## BOM temporaire: Hardware Rev A
Histoire de commencer à bidouiller, on a rassemblé le HW suivant:
- MCU: nucleo STM32F446RE. environ 13€ (la board de dev ou puce seule au même prix).
- Écran: waveshare 3.7pouces pour 28$
- Modem: Quectel EC200T 45€ (en module mPCIe + EVB pour commencer, donc un peu plus cher)
- Clavier Blackberry Q20 en kit by solder party 33€

## Tentative de roadmap 
- V0/HW revA: POC
  - objectif: assembler un hw qui pourrait presque marcher, avec écran+modem+clavier. 
  - hw: le minimum vital pour tester les points clés
  - sw: toolchain&boot TockOS ; afficher des choses sur l'EPD ; passer/recevoir appels, envoyer/recevoir SMS
- V1/HW revB: Développement
  - objectif: avoir une plateforme de dev pour explorer et développer les différents bouts
  - hw: intégrer les différents éléments sur un PCB custom (revB), avec potentiellement encore des bouts de devkit, et form factor adéquat pour le debug/dev plus que pour rentrer dans un boitier
  - sw: faire la base du soft avec des trucs qui marchent presque: afficher des trucs, faire une base de téléphone.
- V2/HW revC: complétude
  - objectif: avoir un presque téléphone qui presque marche - même s'il n'est pas encore en boitier
  - hw: rajouter toutes les features dont on a envie au final (audio, power, ...) et faire un pcb plus ou moins au bon form factor pour rentrer das un boitier.
  - sw: implem téléphone+sms plausibles, brouillon d'un autre service
- V3/HW revD: form factor
  - objectif: remplacer nos téléphones par un tockphone dans nos poches.
  - hw: PCB intégré dans une belle boite
  - sw: implems plausibles de plusieurs services
- V4: racheter Samsung une fois qu'on les a ruinés. 

(effectivement. Tout ceci n'est qu'une blague)
## Liens divers
### OS 
- [Blog](https://os.phil-opp.com/) détaillant toutes les étapes pour faire son propre OS en Rust. Pas besoin si TockOS mais intéressant et bien fait. [OsDev](https://wiki.osdev.org/Main_Page), le wiki des amateurs de création de kernel.

### Gaming
TIC-80 ou PICO-8, mais à 0,3 FPS on va pas aller loin.

- Informatique mobile en mode mesh: https://fr.wikipedia.org/wiki/Projet_Serval
- Debugger pour Cortex M: https://pyocd.io/
- https://small-tech.org/about/#small-technology Philo pour un web0 "easy to use, personal, private by default, share alike, peer-to-peer, interoperable, zero knowledge, non-commercial, non-colonial, inclusive". Mais en node.js.
- https://blog.japaric.io/microamp/ bibliothèque rust minimaliste pour utiliser les MCU multicores; https://rtic.rs/1/book/en/ un framework construit par dessus

