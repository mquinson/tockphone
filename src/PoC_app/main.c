/* Tockphone Proof of Concept App.
 * 
 * Core features of the tockphone, to proof of concept how things work.
 * ultra basic calls and SMS features. nothing fancy.
 *
 * For now this is just blink with some comments.
 * Watch it grow into something!
 */

#include <stdio.h>
#include <strings.h>
#include <led.h>
#include <timer.h>
#include <console_modem.h>

//#include <modem.h> 		// doesnt exist yet!
//#include <keyboard.h> 	// doesnt exist yet!

/* Application FSM:
- display menu: "press c to call, press a to answer, press r to read sms, s to send sms, esc to back to this menu" 

- if received r: 
   query the modem to list received sms in sim
   display them onto console
   ask to select one
   Display selected one
   Ask to delete or back 

- if received s,
     ask to input target number, 
     show on console and ask to validate/correct 
     Ask to type text
     Show and validate to send 

- if received c
    Ask to input target number
    Show on console and validate
    Launch call 

- a is when incoming call, to  answer

*/
void displayMainMenu(void);

void displayMainMenu(void) {
  printf("press c to call, press a to answer, press r to read sms, s to send sms, esc to back to this menu");
}

int main(void) {
  printf("Smolphone PoC App");

  /* Check if the init went all good */
  //TODO check keyboard

  //TODO isQuectel up
  printf("Quectel module booted");

  //TODO setup RI&event management
  printf("voice call manager ready");

  //TODO isCPIN OK
  printf("SIM PIN OK");

  //TODO isNetwork OK
  printf("Network OK");

  /* Simple App FSM */
  for(;;) {
    displayMainMenu();
#if 0
    //test with usb-uart cable on uart1 eg pb6 pb7
    modem_putnstr("hello world",strlen("hello world"));
    delay_ms(2000);//TODO remove this later
    uint8_t inByte = modem_getch();
//  TODO TOCK_FAIL undefined?
//    if (TOCK_FAIL == inByte) {
//       //panic?
//       printf("tockfail in inByte");
//    }
//    else {
       printf("char received: %c\n",inByte);
//    }
#endif
#if 1
    //loopback test on uart1
    
    uint8_t outByte = "a";
    uint8_t inByte;

    modem_putnstr(outByte,1);
    inByte = modem_getch();
    printf("UART1 test, sent: %c received: %c",outByte,inByte);
    outByte++;
    delay_ms(1000);

#endif


    //TODO wait on keyboard input
    /* switch(received character)
    case("r"):  //read SMS
      // query the modem to list received sms in sim
      // display them onto console
      // ask to select one sms
      // display selected one
      // ask to delete or back to main menu

    case("s"):  // send SMS
      // ask to input target number
      // show live input on console and display how to validate/correct/esc with given key
      // ask to type SMS text and display hoy to validate/correct/esc with given key

    case("c"):  // voice call
      // ask to input target number
      // show live input on console and display how to validate/correct/esc with given key
      // launch with ATD, display status + hangup key

    case("a"):  // answer call
      // on RI event switch back to incoming call info on terminal
      // if a pressed when incoming call, answer + hangup key

     */

  }







  // Ask the kernel how many LEDs are on this board.
  int num_leds;
  int err = led_count(&num_leds);
  if (err < 0) return err;

  // Blink the LEDs in a binary count pattern and scale
  // to the number of LEDs on the board.
  for (int count = 0; ; count++) {
    for (int i = 0; i < num_leds; i++) {
      if (count & (1 << i)) {
        led_on(i);
      } else {
        led_off(i);
      }
    }

    // This delay uses an underlying timer in the kernel.
    delay_ms(1000);
  }
}
